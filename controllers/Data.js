const api = require('./api')

module.exports = class Data {
  constructor(mongo) {
    this.mongo = mongo
    this.mergePlayerWithMatches = this.mergePlayerWithMatches.bind(this)
  }

  getChampions() {
    return this.mongo.getChampions()
  }

  getSpells() {
    return this.mongo.getSpells()
  }

  getPerks() {
    return this.mongo.getPerks()
  }

  getPerk(id) {
    return this.mongo.getPerk(id)
  }

  getPlayer(type, queue, forced = false) {
    if(forced) {
      return this.updatePlayer(type, queue).then(this.mergePlayerWithMatches)
    } else {
      return this.mongo.getPlayer(type, queue).then(data => {
        if(data) {
          return this.mergePlayerWithMatches(data)
        }
        else {
          return this.updatePlayer(type, queue).then(this.mergePlayerWithMatches)
        }
      })
    }
  }

  mergePlayerWithMatches(player) {
    let mergePromises = []
    player.matches.forEach(match => mergePromises.push(this.getMatch(match).then(fullMatch => match = fullMatch)))

    return Promise.all(mergePromises).then((values) => {
      player.matches = values
      return player
    })
  }

  getMatch(match) {
    return this.mongo.getMatch(match.platformId, match.gameId)
  }

  insertMatch(match) {
    match.participants.forEach(participant => participant.summoner = match.participantIdentities.filter(account => account.participantId == participant.participantId)[0].player)
    delete match.participantIdentities
    return this.mongo.insertMatch(match)
  }

  updateMatches(matches) {
    let matchesPromises = []
    matches.forEach(match => {
      matchesPromises.push(this.getMatch(match)
      .then(data => {
        if(data) {
          return data
        } else {
          return api.matches.id(match.gameId).then(data => {
            match = data
            return this.insertMatch(match).then(() => {
              return match
            })
          })
        }
      }))
    })

    return Promise.all(matchesPromises)
  }

  updatePlayer(type, queue) {
    let func = null
    switch(type) {
      case 'name':
        func = api.summoner.name
        break
      case 'id':
        func = api.summoner.id
        break
      case 'puuid':
        func = api.summoner.puuid
        break
    }

    if(func) return func(queue)
      .then((user) => {
        return Promise.all([api.mastery.list(user.id), api.league.summoner(user.id), api.matches.account(user.accountId)]).then(values => {
          const mastery = values[0],
                leagues = values[1],
                matches = values[2].matches.splice(0, 10),
                solo = leagues.filter(league => league.queueType === 'RANKED_SOLO_5x5')[0],
                flex = leagues.filter(league => league.queueType === 'RANKED_FLEX_SR')[0],
                tt = leagues.filter(league => league.queueType === 'RANKED_FLEX_TT')[0]

          return Promise.all([this.updateMatches(matches), this.mongo.getChampion('id', mastery[0].championId)])
          .then(values => {
            let matches = values[0],
                champion = values[1]


            let player = {
              lastUpdate: (new Date).getTime(),
              name: user.name,
              id: user.id,
              accountId: user.accountId,
              puuid: user.puuid,
              level: user.summonerLevel,
              profileicon: user.profileIconId,
              champion: {
                splashArt: champion.id
              },
              leagues: {
                solo: solo? {
                  tier: solo.tier,
                  rank: solo.rank,
                  wins: solo.wins,
                  losses: solo.losses,
                  pdl: solo.leaguePoints
                } : null,
                flex: flex? {
                  tier: flex.tier,
                  rank: flex.rank,
                  wins: flex.wins,
                  losses: flex.losses,
                  pdl: flex.leaguePoints
                } : null,
                tt: tt? {
                  tier: tt.tier,
                  rank: tt.rank,
                  wins: tt.wins,
                  losses: tt.losses,
                  pdl: tt.leaguePoints
                } : null
              },
              matches: matches.filter(match => match).filter(match => match.gameId).map(match => {
                return { gameId: match.gameId, platformId: match.platformId }
              })
            }

            return this.mongo.updatePlayer(player).then(() => player)
          })
        })
      })
    else throw new Error('bla')
  }
}
