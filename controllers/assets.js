const rp = require('request-promise')

module.exports = {
  profileIcon(id) {
    return `${global.config.assets}/profile-icon/by-id/${id}`
  },
  champions() {
    let baseURI = global.config.patch.cdn
    return rp({
      method: 'GET',
      json: true,
      uri: `${baseURI}/${global.config.patch.dd}/data/en_US/champion.json`
    })
  },
  spells() {
    let baseURI = global.config.patch.cdn
    return rp({
      method: 'GET',
      json: true,
      uri: `${baseURI}/${global.config.patch.dd}/data/en_US/summoner.json`
    })
  },
  perks() {
    let baseURI = global.config.patch.cdn
    return rp({
      method: 'GET',
      json: true,
      uri: `${baseURI}/${global.config.patch.dd}/data/en_US/runesReforged.json`
    })
  },
  champion: {
    splashArt(name, skin=0) {
      let baseURI = global.config.patch.cdn
      return `${baseURI}/img/champion/splash/${name}_${skin}.jpg`
    },
    loading(name, skin=0) {
      let baseURI = global.config.patch.cdn
      return `${baseURI}/img/champion/loading/${name}_${skin}.jpg`
    },
    icon(name) {
      let baseURI = global.config.patch.cdn
      return `${baseURI}/${global.config.patch.dd}/img/champion/${name}.png`
    }
  },
  item(id) {
    let baseURI = global.config.patch.cdn
    return `${baseURI}/${global.config.patch.dd}/img/item/${id}.png`
  },
  spell(id) {
    let baseURI = global.config.patch.cdn
    return `${baseURI}/${global.config.patch.dd}/img/spell/${id}.png`
  },
  perk(id) {
    let baseURI = global.config.patch.cdn
    return global.data.getPerk(id).then(perk => `${baseURI}/img/${perk.icon}`)
  }
}
