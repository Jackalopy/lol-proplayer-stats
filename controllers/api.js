const rp = require('request-promise')

let options = {
  method: 'GET',
  json: true
}

let baseURI = `https://br1.api.riotgames.com/lol/`
let baseCDN = `https://cdn.communitydragon.org/`

module.exports = {
  mastery:{
    list(id) {
      options.uri = `${baseURI}champion-mastery/v4/champion-masteries/by-summoner/${id}?api_key=${global.config.key}`
      return rp(options)
    },
    individual(id, champion) {
      options.uri = `${baseURI}champion-mastery/v4/champion-masteries/by-summoner/${id}/by-champion/${champion}?api_key=${global.config.key}`
      return rp(options)
    },
    total(id) {
      options.uri = `${baseURI}champion-mastery/v4/scores/by-summoner/${id}?api_key=${global.config.key}`
      return rp(options)
    }
  },
  champions: {
    getData(id) {
      options.uri = `${baseCDN}${global.config.patch.n.champion}/champion/${id}/data`
      return rp(options)
    },
    rotation() {
      options.uri = `${baseURI}platform/v3/champion-rotations?api_key=${global.config.key}`
      return rp(options)
    }
  },
  league: {
    challengers(queue) {
      switch(queue) {
        case 'SOLO' : queue = 'RANKED_SOLO_5x5'
          break
        case 'FLEX' : queue = 'RANKED_FLEX_SR'
          break
        case 'TT' : queue = 'RANKED_FLEX_TT'
          break
      }
      options.uri = `${baseURI}league/v4/challengerleagues/by-queue/${queue}?api_key=${global.config.key}`
      return rp(options)
    },
    grandmasters(queue) {
      switch(queue) {
        case 'SOLO' : queue = 'RANKED_SOLO_5x5'
          break
        case 'FLEX' : queue = 'RANKED_FLEX_SR'
          break
        case 'TT' : queue = 'RANKED_FLEX_TT'
          break
      }
      options.uri = `${baseURI}league/v4/grandmasterleagues/by-queue/${queue}?api_key=${global.config.key}`
      return rp(options)
    },
    masters(queue) {
      switch(queue) {
        case 'SOLO' : queue = 'RANKED_SOLO_5x5'
          break
        case 'FLEX' : queue = 'RANKED_FLEX_SR'
          break
        case 'TT' : queue = 'RANKED_FLEX_TT'
          break
      }
      options.uri = `${baseURI}league/v4/masterleagues/by-queue/${queue}?api_key=${global.config.key}`
      return rp(options)
    },
    summoner(id) {
      options.uri = `${baseURI}league/v4/entries/by-summoner/${id}?api_key=${global.config.key}`
      return rp(options)
    },
    list(queue, tier, division) {
      switch(queue) {
        case 'SOLO' : queue = 'RANKED_SOLO_5x5'
          break
        case 'FLEX' : queue = 'RANKED_FLEX_SR'
          break
        case 'TT' : queue = 'RANKED_FLEX_TT'
          break
      }
      options.uri = `${baseURI}league/v4/entries/${queue}/${tier}/${division}?api_key=${global.config.key}`
      return rp(options)
    },
    leagueId(uuid) {
      options.uri = `${baseURI}league/v4/leagues/${uuid}?api_key=${global.config.key}`
      return rp(options)
    }
  },
  status() {
    options.uri = `${baseURI}status/v3/shard-data?api_key=${global.config.key}`
    return rp(options)
  },
  matches: {
    id(id) {
      options.uri = `${baseURI}match/v4/matches/${id}?api_key=${global.config.key}`
      return rp(options)
    },
    account(id) {
      options.uri = `${baseURI}match/v4/matchlists/by-account/${id}?api_key=${global.config.key}`
      return rp(options)
    },
    timeline(id) {
      options.uri = `${baseURI}match/v4/timelines/by-match/${id}?api_key=${global.config.key}`
      return rp(options)
    },
    tournament(code) {
      options.uri = `${baseURI}match/v4/matches/by-tournament-code/${code}/ids?api_key=${global.config.key}`
      return rp(options)
    },
    tournamentMatch(code, id) {
      options.uri = `${baseURI}match/v4/matches/${id}/by-tournament-code/${code}?api_key=${global.config.key}`
      return rp(options)
    }
  },
  spectator: {
    featured() {
      options.uri = `${baseURI}spectator/v4/featured-games?api_key=${global.config.key}`
      return rp(options)
    },
    summoner(id) {
      options.uri = `${baseURI}spectator/v4/active-games/by-summoner/${id}?api_key=${global.config.key}`
      return rp(options)
    }
  },
  summoner: {
    account(id) {
      options.uri = `${baseURI}summoner/v4/summoners/by-account/${id}?api_key=${global.config.key}`
      return rp(options)
    },
    name(name) {
      name = encodeURIComponent(name)
      options.uri = `${baseURI}summoner/v4/summoners/by-name/${name}?api_key=${global.config.key}`
      return rp(options)
    },
    puuid(id) {
      options.uri = `${baseURI}summoner/v4/summoners/by-puuid/${id}?api_key=${global.config.key}`
      return rp(options)
    },
    id(id) {
      options.uri = `${baseURI}summoner/v4/summoners/${id}?api_key=${global.config.key}`
      return rp(options)
    },
  },
  thirdParty(id) {
    options.uri = `${baseURI}platform/v4/third-party-code/by-summoner/${id}?api_key=${global.config.key}`
    return rp(options)
  }
}
