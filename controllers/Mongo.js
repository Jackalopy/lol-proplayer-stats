const assets = require('./assets'),
  rp = require('request-promise'),
  MongoClient = require('mongodb').MongoClient

module.exports = class Mongo {
  constructor(mongoConfig) {
    this.mongoConfig = mongoConfig
    this.patchUrl = 'https://ddragon.leagueoflegends.com/realms/br.json'
  }

  setup() {
    return MongoClient.connect(this.mongoConfig.url, {useNewUrlParser: true}).then(client => {
      this.db = client.db(this.mongoConfig.name),
      this.config = this.db.collection('config')
      this.champions = this.db.collection('champions')
      this.spells = this.db.collection('spells')
      this.players = this.db.collection('players')
      this.matches = this.db.collection('matches'),
      this.perks = this.db.collection('perks')

      return this.config.findOne({_id: 0})
        .then(res => {
          if(res) global.config.patch = JSON.parse(res.patch)
          return this.update()
        })
    })
  }

  update() {
    rp(this.patchUrl)
    .then(patch => {
      global.config.patch = JSON.parse(patch)
      return Promise.all([assets.champions(), assets.spells(), assets.perks()]).then(values => {
        const champions = values[0],
          spells = values[1],
          perks = values[2],
          championList = [], spellList = [], perkList = []

        Object.entries(champions.data).forEach(champion => {
          let champ = champion[1]
          champ._id = champ.key
          championList.push(champ)
        })

        Object.entries(spells.data).forEach(spell => {
          let sp = spell[1]
          sp._id = sp.key
          spellList.push(sp)
        })

        perks.forEach(tree => {
          tree._id = tree.id
          perkList.push(tree)
          tree.slots.forEach(slot => slot.runes.forEach(rune => {
            rune._id = rune.id
            perkList.push(rune)
          }))
        })

        return Promise.all([
          this.config.updateOne({_id: 0}, {$set:{patch}}, {upsert: true})]
          .concat(championList.map(champion => this.champions.updateOne({_id: champion._id}, {$set: champion}, {upsert: true})))
          .concat(spellList.map(spell => this.spells.updateOne({_id: spell._id}, {$set: spell}, {upsert: true})))
          .concat(perkList.map(perk => this.perks.updateOne({_id: perk._id}, {$set: perk}, {upsert: true})))
        ).catch(console.error)
      })
    })
  }

  getChampion(type, queue) {
    switch(type) {
      case 'id':
        return this.champions.findOne({key: `${queue}`})
      case 'name':
        return this.champions.findOne({name: new RegExp(`^${queue}$`, 'i')})
    }
  }

  getChampions() {
    return this.champions.find({}, {projection: {_id: 0, id: 1, name: 1, key: 1}}).toArray()
  }

  getSpells() {
    return this.spells.find({}, {projection: {_id: 0, id: 1, name: 1, key: 1}}).toArray()
  }

  getPerks() {
    return this.perks.find({}, {projection: {_id: 0, id: 1, name: 1}}).toArray()
  }

  getPerk(id) {
    return this.perks.findOne({id: Number(id)}, {projection: {_id: 0, id: 1, name: 1, icon: 1}})
  }

  getPlayer(type, queue) {
    switch(type) {
      case 'puuid':
        return this.players.findOne({puuid: `${queue}`}, {projection: {_id: 0}})
      case 'name':
        return this.players.findOne({name: new RegExp(`^${queue}$`, 'i')}, {projection: {_id: 0}})
    }
  }

  updatePlayer(player) {
    if(player.puuid) {
      return this.players.updateOne({_id: player.puuid}, {$set: player}, {upsert: true})
    } else {
      throw 'error, no puuid'
    }
  }

  getMatch(platformId, gameId) {
    return this.matches.findOne({platformId, gameId})
  }

  insertMatch(match) {
    match._id = `${match.platformId}_${match.gameId}`
    return this.matches.insertOne(match)
  }
}
