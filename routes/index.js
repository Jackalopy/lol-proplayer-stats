const api = require('../controllers/api'),
      assets = require('../controllers/assets')

module.exports = function(app, data) {

  app.route('/data/summoner/by-name')
    .get((req, res) => {
      let name = req.query.name
      console.log(name)
      data.getPlayer('name', name)
        .then(player => res.send(player))
        .catch(console.error)
    })

  app.route('/data/summoner/by-puuid')
    .get((req, res) => {
      let puuid = req.query.puuid,
          forced = req.query.forced

      data.getPlayer('puuid', puuid, true)
        .then((player) => res.send(player))
        .catch(console.error)
    })

  app.route('*')
    .get((req, res) => {
      Promise.all([data.getChampions(), data.getSpells()])
      .then(values => {
        let champions = values[0],
        spells = values[1]

        res.render('index', {config: {
          title: 'Ganqs - ProPlayer Stats',
          description: 'Ohhhh yeaaahh',
          favicon: '/assets/img/favicon.png',
          assets: global.config.assets
        }})
      })
    })
}
