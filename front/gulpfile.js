let fs					= require('fs'),
		gulp 				= require('gulp'),
		gulpPug 		= require('gulp-pug'),
		stylus 			= require('gulp-stylus'),
		sourcemaps 	= require('gulp-sourcemaps'),
		browserify 	= require('browserify'),
		tsify				= require('tsify')
		stream			= require('gulp-streamify'),
		uglify			= require('gulp-uglify'),
		source			= require('vinyl-source-stream'),
		babel				= require('babelify')

const config = fs.readFileSync('../config.json')

function ts() {
	return browserify({
        basedir: '.',
        debug: true,
        entries: ['src/script/main.tsx'],
        cache: {},
        packageCache: {}
    })
		.plugin(tsify)
		.bundle()
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('./src/js'))
}

function js() {
	let bundle = browserify('./src/script/main.js')
		.transform(babel, {
			presets: ['@babel/preset-env', '@babel/preset-react'],
			plugins: [['import', {
				libraryName: 'antd'
			}]],
			sourceMaps: true
		})
		.bundle()
		.on('error', swallowError)

	let pipes = [bundle.pipe(fs.createWriteStream('./build/assets/js/bundle.js'))]

	if(config.env === 'prod') {
		pipes.push(bundle
			.pipe(source('bundle.min.js'))
			.pipe(stream(uglify()))
			.pipe(gulp.dest('./build/assets/js/'))
			.on('error', swallowError))
	}

	return Promise.all(pipes)

}

function css() {
    return gulp.src('./src/style/bundle.styl')
        .pipe(sourcemaps.init())
        .pipe(stylus({
					'include css': true
				}))
        .on('error', swallowError)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./build/assets/css/'));
}

function fonts() {
	return gulp.src('./src/fonts/**/*')
		.pipe(gulp.dest('./build/assets/fonts'));
}

function imgs() {
	return gulp.src('./src/img/**/*')
		.pipe(gulp.dest('./build/assets/img'));
}

function pug() {
	return gulp.src('./src/view/*.pug')
	// .pipe(gulpPug({}))
	.on('error', swallowError)
	.pipe(gulp.dest('./build/view'));
}

function watch() {
	gulp.watch(['./src/**/*.js', './src/**/*.jsx'], js)
	gulp.watch(['./src/**/*.styl'], css)
	gulp.watch(['./src/fonts/**/*'], fonts)
	gulp.watch(['./src/img/**/*'], imgs)
	gulp.watch(['./src/**/*.pug'], pug)
}

exports.build = gulp.series(gulp.parallel(js, css, fonts, imgs, pug))
exports.watch = gulp.series(watch)
exports.default = gulp.series(gulp.parallel(js, css, fonts, imgs, pug), watch)


function swallowError (error) {
  console.log('',error.toString(), '')
  this.emit('end')
}
