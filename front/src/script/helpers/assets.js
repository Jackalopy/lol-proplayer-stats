const rp = require('request-promise')

export default {
  img(type, filter, queue) {
    return `${window.data.assets}/${type}/by-${filter}/${queue}`
  },
  data(type) {
    switch(type) {
      case 'version':
        return rp(`${window.data.assets}/data/${type}`)
        break

      default:
        return rp(`${window.data.assets}/data/${type}`).then(JSON.parse)
        break
    }
  },
  static(type, queue) {
    return `/assets/${type}/${queue}`
  }
}
