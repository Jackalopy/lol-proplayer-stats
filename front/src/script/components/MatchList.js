import React from 'react'
import ReactDom from 'react-dom'
import { connect } from 'react-redux'
import styled from 'styled-components'

import RoleIcon from './RoleIcon'
import { Layout, Radio, Divider, Select, Typography } from 'antd'
const { Option } = Select

import mapState from '../helpers/mapState'
import Match from './Match'

const Container = styled.div`
  position: relative;
  z-index: 0;
  background-color: #fff;
  box-shadow: 0 1px 2px rgba(0,0,0,0.2);
  padding: 0;
  margin-bottom: 1em;
`

const SubContainer = styled.div`
  position: relative;
  padding: 1em;
`

const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
`

const Text = styled.p`
  font-size: ${props => props.size? props.size + 'px' : '1em'};
  font-weight: ${props => props.strong? 'bold' : 'normal'};
  color: ${props => props.color? props.color : '#FFFFFF'};;
  margin: 0;
`

class MatchList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      filter: {
        role: {
          selected: '',
          values: [
            {image: 'all', value: ''},
            {image: 'top', value: 'top'},
            {image: 'jg', value: 'jg'},
            {image: 'mid', value: 'mid'},
            {image: 'adc', value: 'adc'},
            {image: 'sup', value: 'sup'}]
        },
        queue: {
          selected: '',
          values: [
            {name: 'All Queues', value: ''},
            {name: 'Ranked Solo', value: 'Ranked Solo'},
            {name: 'Ranked Flex', value: 'Ranked Flex'},
            {name: 'Normal Draft', value: 'Normal Draft'},
            {name: 'Normal Blind', value: 'Normal Blind'},
            {name: 'ARAM', value: 'ARAM'},
            {name: 'Nexus Blitz', value: 'Nexus Blitz'},
            {name: 'Twisted Treeline', value: 'Twisted Treeline'}]
        },
        champion: {selected: ''}
      }
    }

    this.changeRole = this.changeRole.bind(this)
    this.changeQueue = this.changeQueue.bind(this)
    this.changeChampion = this.changeChampion.bind(this)
  }

  changeRole(selected) {
    let temp = Object.assign({}, this.state.filter.role, {selected: selected.target.value})
    this.setState({ filter: Object.assign({}, this.state.filter, {role: temp}) })
  }

  changeQueue(selected) {
    let temp = Object.assign({}, this.state.filter.queue, {selected})
    this.setState({ filter: Object.assign({}, this.state.filter, {queue: temp}) })
  }

  changeChampion(selected) {
    let temp = Object.assign({}, this.state.filter.champion, {selected: selected ? this.props.gameData.champions.filter(champion => champion.name == selected)[0].key : selected})
    this.setState({ filter: Object.assign({}, this.state.filter, {champion: temp}) })
  }

  render() {
    return (
      <Layout.Content style={{maxWidth: 1000}}>
        <SubContainer>
          <Text strong>Match History</Text>
        </SubContainer>
        <SubContainer style={{marginTop: '-1em', padding: '1em 0'}}>
          <Radio.Group defaultValue={this.state.filter.role.selected} onChange={this.changeRole} style={{marginRight: '1em'}}>
            {this.state.filter.role.values.map((role, i) =>
              <Radio.Button key={i} value={role.value}>
                <RoleIcon type={role.image} style={{height: '1em'}}/>
              </Radio.Button>
            )}
          </Radio.Group>
          <Select defaultValue={this.state.filter.queue.selected} onSelect={this.changeQueue} style={{minWidth: 150, marginRight: '1em'}}>
            {this.state.filter.queue.values.map((queue, i) =>
              <Option key={i} value={queue.value}>{queue.name}</Option>
            )}
          </Select>
          <Select
            showSearch
            defaultValue={this.state.filter.champion.selected}
            style={{minWidth: 230}}
            onSelect={this.changeChampion}
          >
            <Option key={-1} value=''>All Champions</Option>
            {this.props.gameData.champions.map((champion, i) =>
              <Option key={i} value={champion.name}>{champion.name}</Option>
            )}
          </Select>

        </SubContainer>
        {this.props.matches
          .map((match, i) =>
            <Match
              key={i}
              match={match}
              player={this.props.player}
              filters={{
                role: this.state.filter.role.selected != ''? this.state.filter.role.selected : null,
                queue: this.state.filter.queue.selected != ''? this.state.filter.queue.selected : null,
                champion: this.state.filter.champion.selected != ''? this.state.filter.champion.selected : null
              }}
            />
          )
        }
      </Layout.Content>
    )
  }
}

export default connect(mapState)(MatchList)
