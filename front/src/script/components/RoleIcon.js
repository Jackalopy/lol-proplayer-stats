import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'

export default class Icon extends React.Component {
  constructor() {
    super()
  }

  render() {
    let icon,
      brightness = this.props.white? 99999 : 0

    switch(this.props.type) {
      case 'all':
        icon = 'all_icon.png'
        break;
      case 'top':
        icon = 'top_icon.png'
        break;
      case 'jg':
        icon = 'jungle_icon.png'
        break;
      case 'mid':
        icon = 'middle_icon.png'
        break;
      case 'adc':
        icon = 'bottom_icon.png'
        break;
      case 'sup':
        icon = 'support_icon.png'
        break;
    }
    return <img src={window.helpers.assets.static('img', icon)} style={Object.assign({}, {filter: `brightness(${brightness})`}, this.props.style)} />
  }
}
