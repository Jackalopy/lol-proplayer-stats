import React from 'react'
import ReactDom from 'react-dom'
import styled from 'styled-components'
import { connect } from 'react-redux'

const Flex = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 100%;
`

const Logo = styled.img`
  display: block;
  width: 500px;
  max-width: 100%;
`

const Text = styled.p`
  color: rgba(255, 255, 255, .5);
  width: 500px;
  text-align: center;
  margin: 1em 0;
`

const mapState = state => Object.assign({}, {gameData: state.gameData})

export default class Home extends React.Component{
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Flex>
        <Logo src={window.helpers.assets.static('img', 'ganqs_logo.svg')}/>
        <Text>
          <span style={{fontWeight: 'bold'}}>Ganqs.com</span> isn’t endorsed by Riot Games and doesn’t reflect the views or opinions of Riot Games
or anyone officially involved in producing or managing League of Legends. League of Legends and Riot Games are
trademarks or registered trademarks of Riot Games, Inc. League of Legends © Riot Games, Inc.
        </Text>
      </Flex>
    )
  }
}
