import React from 'react'
import ReactDom from 'react-dom'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { Layout, Typography, Divider, Avatar, Badge, Button, Icon } from 'antd'
import TimeAgo from 'javascript-time-ago'
import en from 'javascript-time-ago/locale/en'
import rp from 'request-promise'

import MatchList from '../MatchList'
import mapState from '../../helpers/mapState'

const { Paragraph } = Typography

TimeAgo.addLocale(en)
const timeAgo = new TimeAgo('en-US')

const Container = styled.div`
  position: relative;
  z-index: 0;
  padding: 0;
  margin-bottom: 1em;
`

const SubContainer = styled.div`
  position: relative;
  padding: 1em;
`

const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
`

const FlexCol = styled.div`
  display: flex;
  flex-direction: column;
`

const Profile = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding-left: 1em;
`

const Background = styled.div`
  position: absolute;
  top: -64px;
  left: 50%;
  z-index: -1;
  transform: translateX(-50%);
  height: 500px;
  width: 1100px;
  background: rgb(40, 51, 61);
  overflow: hidden;

  :before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: -1;

    ${props => `
      background-position: 50% 10%;
      background-size: cover;
      opacity: ${props.opacity}
      background-image: url(${props.bg});
      `}
  }

  :after {
    content: "";
    position: absolute;
    top: -100%;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 0;
    background: radial-gradient(transparent 0, rgb(40, 51, 61) 70%)
  }
`

const Leagues = styled.ul`
  display: flex;
  flex-direction: row;
  padding: 0;
  margin: 0;
  pointer-events: none;
  list-decoration: none;
`

const League = styled.li`
  display: flex;
  flex-direction: column;
  min-width: 80px;
  margin-right: 1em;
`

const Text = styled.p`
  display: inline-block;
  font-size: ${props => props.size? props.size + 'px' : '1em'};
  font-weight: ${props => props.strong? 'bold' : 'normal'};
  color: ${props => props.color? props.color : '#FFFFFF'};;
  margin: 0;
`

const Title = styled.div`
  font-size: ${props => props.size? props.size + 'px' : '20px'};
  font-weight: bold;
  color: ${props => props.color? props.color : '#FFFFFF'};;
  margin: 0;
`

export default class Summoner extends React.Component {
  constructor(props) {
    super(props)

    this.state= {
      user: null
    }

    this.getPlayer = this.getPlayer.bind(this)
    this.updatePlayer = this.updatePlayer.bind(this)
  }

  componentDidMount() {
    if(this.props.player) this.getPlayer(this.props.player)
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.player !== this.props.player) {
      this.getPlayer(this.props.player)
    }
  }

  getPlayer(player) {
    return rp({
      uri: `${document.location.origin}/data/summoner/by-name`,
      json: true,
      qs: {
        name: player,
      }
    }).then((data) => {
      this.setState({player: data})
    })
  }

  updatePlayer() {
    return rp({
      uri: `${document.location.origin}/data/summoner/by-puuid`,
      json: true,
      qs: {
        puuid: this.state.player.puuid,
        forced: true
      }
    }).then((data) => this.setState({player: data}))
  }

  render() {
    if(this.state.player) {
      let player = this.state.player
      return (
        <Layout.Content>
          <Container>
            <FlexRow>
              <Avatar shape="square" size={100} icon="user" src={window.helpers.assets.img('profile-icon', 'id', player.profileicon)}/>
              <Profile>
                <Title level={4} style={{margin: 0}}>
                  {player.name}
                  <Text type="secondary" style={{marginLeft: '.5em', fontSize: '.7em'}}>Level {player.level}</Text>
                </Title>
                <Leagues>
                  <League>
                    <Text style={{fontSize: '.8em'}}>Solo</Text>
                    <Text strong>
                      {player.leagues.solo? `${player.leagues.solo.tier} ${player.leagues.solo.rank}` : 'Unranked'}
                    </Text>
                    {player.leagues.solo?
                      <Text style={{fontSize: '.8em'}}>WINRATE {Math.round(player.leagues.solo.wins/(player.leagues.solo.wins + player.leagues.solo.losses)*100, 1)}%</Text>
                    :''}
                    {player.leagues.solo?
                      <Text type="secondary" style={{fontSize: '.8em'}}>{player.leagues.solo.pdl} PDL</Text>
                    :''}
                    </League>
                    <League>
                      <Text style={{fontSize: '.8em'}}>Flex</Text>
                      <Text strong>
                        {player.leagues.flex? `${player.leagues.flex.tier} ${player.leagues.flex.rank}` : 'Unranked'}
                      </Text>
                      {player.leagues.flex?
                        <Text style={{fontSize: '.8em'}}>WINRATE {Math.round(player.leagues.flex.wins/(player.leagues.flex.wins + player.leagues.flex.losses)*100, 1)}%</Text>
                      :''}
                      {player.leagues.flex?
                        <Text type="secondary" style={{fontSize: '.8em'}}>{player.leagues.flex.pdl} PDL</Text>
                      :''}
                    </League>
                    <League>
                      <Text style={{fontSize: '.8em'}}>Twister Treeline</Text>
                      <Text strong>
                        {player.leagues.tt? `${player.leagues.tt.tier} ${player.leagues.tt.rank}` : 'Unranked'}
                      </Text>
                      {player.leagues.tt?
                        <Text style={{fontSize: '.8em'}}>WINRATE {Math.round(player.leagues.tt.wins/(player.leagues.tt.wins + player.leagues.tt.losses)*100, 1)}%</Text>
                      :''}
                      {player.leagues.tt?
                        <Text type="secondary" style={{fontSize: '.8em'}}>{player.leagues.tt.pdl} PDL</Text>
                      :''}
                    </League>
                  </Leagues>
                </Profile>
              <FlexCol style={{position: 'absolute', right: '0', alignItems: 'flex-end'}}>
                <Button style={{background: 'transparent', color: '#fff'}} onClick={this.updatePlayer}><Icon type="reload" /></Button>
                <Text strong style={{fontSize: '.8em', marginTop: '.5em'}}>Last Update:</Text>
                <Text style={{fontSize: '.8em'}}>{timeAgo.format(new Date(player.lastUpdate))}</Text>
              </FlexCol>
            </FlexRow>
          </Container>
          <MatchList player={player} matches={player.matches} />
          <Background bg={window.helpers.assets.img('champion-splash', 'name', player.champion.splashArt)} opacity={.5}/>
        </Layout.Content>
      )
    } else {
      return 'No Summoner'
    }
  }
}
