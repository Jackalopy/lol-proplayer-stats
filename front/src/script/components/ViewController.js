import React from 'react'
import ReactDOM from 'react-dom'
import { Layout } from 'antd'
import SummonerContent from './contents/Summoner'


export default class ViewController extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const children = this.props.children

    let view = React.Children.map(children, (child => {
      if(child.props.viewKey === this.props.view)
        return child
      else return
    }))
    return (
      <Layout.Content style={{
          position: 'relative',
          zIndex: 0,
          display: 'table',
          width: 1000,
          maxWidth: '100%',
          minHeight: 'calc(100% - 100px)',
          margin: '0 auto',
          padding: 50,
          backgroundColor: 'rgb(40, 51, 61)'
      }}>
        {view}
      </Layout.Content>
    )
  }
}
