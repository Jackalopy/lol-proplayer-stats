import React from 'react'
import ReactDOM from 'react-dom'

import SearchBar from './SearchBar'
import { Menu, Icon } from 'antd'

export default class _Menu extends React.Component {
  constructor() {
    super()
  }

  render() {
    return (
      <Menu
        mode="vertical"
        theme="dark"
        mode="inline"
        onSelect={this.props.onSelect}
        style={{
          background: 'transparent'
        }}
      >
        <Menu.Item key="summoner">
          <Icon type="user" />
          <span>Summoner</span>
        </Menu.Item>
      </Menu>
    )
  }
}
