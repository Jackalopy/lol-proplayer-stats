import React from 'react'
import ReactDOM from 'react-dom'
import { Input, Select, AutoComplete } from 'antd'

const { Option } = Select

let ref = React.createRef()

export default class SearchBar extends React.Component {
  constructor() {
    super()
    this.state = {
      region: 'br'
    }
    this.changeRegion = this.changeRegion.bind(this)
  }

  changeRegion(region) {
    this.setState({region})
  }

  render() {
    return (
      <div>
        <AutoComplete
          ref={ref}
          dataSource={this.props.dataSource}
          filterOption={(inputValue, option) =>
            option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
          }
          onSelect={value => this.props.onSearch(value, this.state.region)}
          >
          <Input.Search
            placeholder="Search for Player"
            style={{width: 300}}
            onSearch={value => this.props.onSearch(value, this.state.region)}
            />
        </AutoComplete>
        <Select defaultValue={this.state.region} onChange={this.changeRegion} style={{marginLeft: 10}}>
          <Option value="br">Brazil</Option>
          <Option value="na" disabled>North America</Option>
          <Option value="eu" disabled>Europe</Option>
        </Select>
      </div>)
  }
}
