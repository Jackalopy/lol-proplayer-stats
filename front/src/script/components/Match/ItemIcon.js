import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import Icon from './Icon'
import { Parser } from 'html-to-react'
const reactParser = (new Parser()).parse

export default class ItemIcon extends React.Component {
  render() {
    let item = this.props.item
    let src = item? window.helpers.assets.img('item', 'id', item.key) : null
    if(item)
      return (
        <Icon
          popover={this.props.popover}
          shape="circle"
          size={this.props.size}
          src={src}
          style={this.props.style}
          title={item.name}
          content={reactParser(item.description)}
        />)
      else
        return (
          <Icon
            popover={false}
            shape="circle"
            size={this.props.size}
            src={src}
            style={this.props.style}
          />)
  }
}
