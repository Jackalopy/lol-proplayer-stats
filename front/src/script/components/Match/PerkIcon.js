import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import Icon from './Icon'
import { Parser } from 'html-to-react'
const reactParser = (new Parser()).parse

export default class PerkIcon extends React.Component {
  render() {
    let perk = this.props.perk
    let src = perk? window.helpers.assets.img('perk', 'id', perk.id) : null
    if(perk)
      return (
        <Icon
          popover={this.props.popover}
          shape="circle"
          size={this.props.size}
          src={src}
          style={this.props.style}
          title={perk.name}
          content={reactParser(perk.longDesc)}
        />)
      else
        return (
          <Icon
            popover={false}
            shape="circle"
            size={this.props.size}
            src={src}
            style={this.props.style}
          />)
  }
}
