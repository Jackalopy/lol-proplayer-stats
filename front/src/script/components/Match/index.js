import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import styled from 'styled-components'
import TimeAgo from 'javascript-time-ago'
import en from 'javascript-time-ago/locale/en'
import moment from 'moment'

import ChampionPic from './ChampionPic'
import RoleIcon from '../RoleIcon'
import PerkIcon from './PerkIcon'
import PlayerIcon from './PlayerIcon'
import ChampionIcon from './ChampionIcon'
import ItemIcon from './ItemIcon'
import SpellIcon from './SpellIcon'

import mapState from '../../helpers/mapState'

TimeAgo.addLocale(en)
const timeAgo = new TimeAgo('en-US')

const Container = styled.div`
  position: relative;
  z-index: 0;
  background-color: #fff;
  box-shadow: 0 1px 2px rgba(0,0,0,0.2);
  padding: 0;
  margin-bottom: 1em;
`

const SubContainer = styled.div`
  position: relative;
  padding: .5em;
`

const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: ${props => props.align? props.align : 'center'};
  justify-content: ${props => props.justify? props.justify : 'center'};
`

const FlexCol = styled.div`
  display: flex;
  flex-direction: column;
  align-items: ${props => props.align? props.align : 'flex-start'};
  justify-content: ${props => props.justify? props.justify : 'center'};
`

const Game = styled.div`
  position: relative;
  z-index: 0;
  display: flex;
  flex-direction: row;
  justify-items: space-between;
  min-width: 750px;
  padding: 0 1em;
  margin: 2px 0;
  font-size: .8em;
  transition: all .3s;
  background-color: #11151A;

  :hover {
    background-color: #171D24;
  }
`

const Stats = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  justify-content: space-between;
  padding: 1em
`

const Text = styled.p`
  font-size: ${props => props.size? props.size + 'px' : '1em'};
  font-weight: ${props => props.strong? 'bold' : 'normal'};
  color: ${props => props.color? props.color : '#FFFFFF'};;
  margin: 0;
`

const VictoryBar = styled.div`
  position: absolute;
  z-index: 1;
  left: 0;
  top: 50%;
  transform: translate(-50%, -50%);
  border-radius: 100px;
  width: 4px;
  height: 50%;
  ${props =>
    `background-color: ${props.color};`
  }
`

class Match extends React.Component {
  constructor(props) {
    super(props)

    this.state={
      hover: false
    }

    this.mouseEnter = this.mouseEnter.bind(this)
    this.mouseLeave = this.mouseLeave.bind(this)
  }

  mouseEnter() {
    this.setState({hover: true})
  }
  mouseLeave() {
    this.setState({hover: false})
  }

  render() {
    let matchData = this.props.match
    if(!matchData) return

    let playerData = null, queue = null, teams = {blue: matchData.participants.filter(player => player.teamId == 100), red: matchData.participants.filter(player => player.teamId == 200)}

    switch(matchData.queueId) {
      case 65:
      case 100:
      case 450:
      case 100:
        queue = 'ARAM'
        break

      case 4:
      case 420:
        queue = 'Ranked Solo'
        break

      case 440:
        queue = 'Ranked Flex'
        break

      case 41:
      case 470:
        queue = 'Ranked Twisted Treeline'
        break

      case 14:
      case 400:
        queue = 'Normal Draft'
        break

      case 2:
      case 430:
        queue = 'Normal Blind'
        break

      case 1200:
        queue = 'Nexus Blitz'
        break

      case 7:
      case 25:
      case 31:
      case 32:
      case 33:
      case 52:
      case 67:
      case 83:
      case 810:
      case 820:
      case 830:
      case 840:
      case 850:
        queue = 'Co-op vs AI'
        break

      default:
        queue = 'Other'
    }

    let min = Math.floor(matchData.gameDuration/60),
        sec = (matchData.gameDuration - min*60)

    matchData.participants.forEach(player => {
      let role = null,
          champion = this.props.gameData.champions.filter(champion => champion.key == player.championId)[0],
          items = [], trinket,
          spells = []


      items.push(player.stats.item0)
      items.push(player.stats.item1)
      items.push(player.stats.item2)
      items.push(player.stats.item3)
      items.push(player.stats.item4)
      items.push(player.stats.item5)
      trinket = player.stats.item6

      spells.push((player.spell1Id))
      spells.push((player.spell2Id))

      if(queue == 'ARAM') role = 'all'
      else switch(player.timeline.lane) {
        case 'TOP':
        role = 'top'
        break
        case 'JUNGLE':
        role = 'jg'
        break
        case 'MIDDLE':
        role = 'mid'
        break
        case 'BOTTOM':
        role = player.timeline.role == 'DUO_SUPPORT'? 'sup' : 'adc'
        break
        case 'NONE':
        role = player.spell1Id == 11 || player.spell2Id == 11? 'jg' : 'all'
        break
        default:
        role = 'all'
      }

      player.treatedData = {
        victory: player.stats.win,
        remake: min < 5,
        length: `${min}:${sec < 10? '0' + sec : sec}`,
        queue,
        side: player.teamId == 100? 'blue' : 'red',
        champion,
        role,
        level: player.stats.champLevel,
        cs: player.stats.totalMinionsKilled + player.stats.neutralMinionsKilled,
        kda: [player.stats.kills, player.stats.deaths, player.stats.assists],
        startTime: matchData.gameCreation,
        spells: spells.map(spellId => spellId? this.props.gameData.spells.filter(spell => spell.key == spellId)[0] : null),
        perks: [this.props.gameData.perks.filter(perk => perk.id == player.stats.perk0)[0], this.props.gameData.perks.filter(perk => perk.id == player.stats.perkSubStyle)[0]],
        items: items.map(itemId => itemId != 0? this.props.gameData.items.filter(item => item.key == itemId)[0] : null),
        trinket: trinket? this.props.gameData.items.filter(item => item.key == trinket)[0] : null,
        teams
      }
      playerData = player.summoner.summonerId === this.props.player.id? player : playerData
    })

    let game = playerData.treatedData

    let gameColor = game.remake? '#536980' : (game.victory? '#2ecc71' : '#e74c3c'),
        sideColor = game.side == 'blue'? '#3498db' : '#e74c3c',
        highlighColor = '#1abc9c'

    if(this.props.filters.champion && this.props.filters.champion != game.champion.key) return ''
    if(this.props.filters.queue && this.props.filters.queue != game.queue) return ''
    if(this.props.filters.role && this.props.filters.role != game.role) return ''

    return (
      <Game victory={game.victory} onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave}>
        <VictoryBar color={gameColor}/>
        <Stats>
          <FlexCol justify={'flex-start'} style={{minWidth: 60}}>
            <Text strong style={{whiteSpace: 'nowrap'}}>{game.queue}</Text>
            <Text style={{textTransform: 'uppercase'}}><span style={{color: sideColor, fontWeight: 'bold'}}>{game.side}</span> Side</Text>
            <Text>{game.length}</Text>
          </FlexCol>
        </Stats>
        <ChampionPic champion={game.champion} role={game.role} hover={this.state.hover}/>
          <Stats style={{width: '100%'}}>
            <FlexCol justify={'space-around'} align={'center'}>
              <FlexCol style={{position: 'relative'}}>
                <PerkIcon popover={true} size={36} perk={game.perks[0]} style={{background: 'rgb(40, 51, 61)'}}/>
                <PerkIcon popover={false} size={18} perk={game.perks[1]} style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(25%, 25%)', background: 'transparent'}}/>
              </FlexCol>
              <FlexRow>
                {game.spells.map((spell, i) => spell? <SpellIcon popover={true} key={i} size={24} spell={spell} style={{margin: '0 .25em'}} /> : '')}
              </FlexRow>
            </FlexCol>
            <FlexCol align={'center'}>
              <Text size={14}>
                <span style={{color:'#e67e22', fontWeight: 700}}> {game.kda[0]} </span> /
                <span style={{color:'#e67e22', fontWeight: 700}}> {game.kda[1]} </span> /
                <span style={{color:'#e67e22', fontWeight: 700}}> {game.kda[2]} </span>
              </Text>
              <Text>
                <span style={{fontWeight: 700}}>{((game.kda[0] + game.kda[2])/(game.kda[1] ? game.kda[1] : 1)).toFixed(1)} </span>
                KDA
              </Text>
            </FlexCol>
            <FlexCol>
              <Text strong>Level {game.level}</Text>
              <Text>{game.cs} ({(game.cs/game.length.replace(':', '.')).toFixed(2)}) CS</Text>
            </FlexCol>
            <FlexCol align="flex-start">
              <FlexRow justify="flex-start" style={{marginBottom: '.2em'}}>
                <ItemIcon popover={true} size={20} item={game.items[0]} style={{backgroundColor: 'rgb(33, 42, 51)', margin: '0 .3em 0 0'}} />
                <ItemIcon popover={true} size={20} item={game.items[1]} style={{backgroundColor: 'rgb(33, 42, 51)', margin: '0 .3em 0 0'}} />
                <ItemIcon popover={true} size={20} item={game.items[2]} style={{backgroundColor: 'rgb(33, 42, 51)', margin: '0 .3em 0 0'}} />
                <ItemIcon popover={true} size={20} item={game.trinket} style={{backgroundColor: 'rgb(33, 42, 51)', margin: '0 .3em 0 0'}} />
              </FlexRow>
              <FlexRow justify="flex-start">
                <ItemIcon popover={true} size={20} item={game.items[3]} style={{backgroundColor: 'rgb(33, 42, 51)', margin: '0 .3em 0 0'}} />
                <ItemIcon popover={true} size={20} item={game.items[4]} style={{backgroundColor: 'rgb(33, 42, 51)', margin: '0 .3em 0 0'}} />
                <ItemIcon popover={true} size={20} item={game.items[5]} style={{backgroundColor: 'rgb(33, 42, 51)', margin: '0 .3em 0 0'}} />
              </FlexRow>
            </FlexCol>
            <FlexCol>
              <FlexRow justify="flex-start" style={{marginBottom: '.2em'}}>
                {game.teams.blue.map((player, i) => player? <PlayerIcon key={i} popover={true} size={20} type={'champion'} player={player} style={{margin: '0 .2em'}} /> : '')}
              </FlexRow>
              <FlexRow justify="flex-start">
                {game.teams.red.map((player, i) => player? <PlayerIcon key={i} popover={true} size={20} type={'champion'} player={player} style={{margin: '0 .2em'}} /> : '')}
              </FlexRow>
            </FlexCol>
            <FlexCol align={'flex-end'} justify={'flex-start'}>
              <Text strong>{timeAgo.format(new Date(game.startTime))}</Text>
              <Text>{moment(game.startTime).format('MMM DD')}</Text>
            </FlexCol>
          </Stats>
      </Game>
    )
  }
}

export default connect(mapState)(Match)
