import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import Icon from './Icon'
import { Parser } from 'html-to-react'
const reactParser = (new Parser()).parse

export default class PerkIcon extends React.Component {
  render() {
    let spell = this.props.spell
    let src = spell? window.helpers.assets.img('spell', 'id', spell.key) : null
    if(spell)
      return (
        <Icon
          popover={this.props.popover}
          shape="circle"
          size={this.props.size}
          src={src}
          style={this.props.style}
          title={spell.name}
          content={reactParser(spell.description)}
        />)
      else
        return (
          <Icon
            popover={false}
            shape="circle"
            size={this.props.size}
            src={src}
            style={this.props.style}
          />)
  }
}
