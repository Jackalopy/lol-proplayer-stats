import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import Icon from './Icon'
import MiniProfile from './MiniProfile'

export default class PlayerIcon extends React.Component {
  render() {
    let src = null
    switch(this.props.type) {
      case 'champion':
        src = window.helpers.assets.img('champion-icon', 'id', this.props.player.championId)
        break
      default:
      case 'summoner':
        src = window.helpers.assets.img('profile-icon', 'id', this.props.player.summoner.profileIcon)
        break
    }

    return (
      <Icon
        shape="square"
        size={this.props.size}
        src={src}
        style={this.props.style}
        title={this.props.player.summoner.summonerName + (this.props.player.summoner.accountId == 0? ' Bot' : '')}
        content={<MiniProfile player={this.props.player}/>}
        popover={this.props.popover}
      />
    )
  }
}
