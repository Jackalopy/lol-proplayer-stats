import React from 'react'
import ReactDOM from 'react-dom'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { Avatar, Popover, Button } from 'antd'
import ItemIcon from './ItemIcon'

export default class MiniProfile extends React.Component {
  render() {
    let player = this.props.player.treatedData,
        items = player.items

    const Line = styled.div`
      display: flex;
      flex-direction: row;
      justify-content: center;
      margin: 1em 0;

      :first-child {
        margin-top: 0
      }

      :last-child {
        margin-bottom: 0
      }
    `
    
    return (
      <div>
        <div>
          <Line style={{justifyContent: 'space-between'}}>
            <span style={{fontWeight: 'bold'}}>{player.champion.name}</span>
            <span>
              <span style={{color:'#e67e22', fontWeight: 700}}> {player.kda[0]} </span>/
              <span style={{color:'#e67e22', fontWeight: 700}}> {player.kda[1]} </span>/
              <span style={{color:'#e67e22', fontWeight: 700}}> {player.kda[2]} </span>
            </span>
          </Line>
          <Line>
            <div style={{width: 'fit-content', margin: '0 auto', background: '#ccc', borderRadius: 100, display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: '.2em'}}>
              {items.map((item, i) => <ItemIcon key={i} popover={false} item={item} size={16} style={{margin: '.2em'}}/>)}
            </div>
          </Line>
          {this.props.player.summoner.accountId != 0?
            <Line style={{}}>
              <Link to={`/summoner/name/${this.props.player.summoner.summonerName}`}>
                <Button>Go to Profile</Button>
              </Link>
            </Line>
            : ''
          }
        </div>
      </div>
    )
  }
}
