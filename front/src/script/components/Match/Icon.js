import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import { Avatar, Popover } from 'antd'

export default class ItemIcon extends React.Component {
  render() {
    let src = this.props.item? window.helpers.assets.img('item', 'id', this.props.item.key) : null
    if(this.props.popover)
      return (
        <Popover title={this.props.title} content={this.props.content}>
          <Avatar shape={this.props.shape} size={this.props.size} icon="stop" src={this.props.src} style={this.props.style}/>
        </Popover>
      )
    else
      return <Avatar shape={this.props.shape} size={this.props.size} icon="stop" src={this.props.src} style={this.props.style}/>
  }
}
