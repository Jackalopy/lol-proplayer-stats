import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import { Avatar } from 'antd'

export default class ChampionIcon extends React.Component {
  render() {
    return (
      <Avatar className={'champion-icon'} key={i} shape="square" size={20} icon="stop" src={window.helpers.assets.img('champion-icon', 'id', this.props.champion.key)} style={this.props.style}/>
    )
  }
}
