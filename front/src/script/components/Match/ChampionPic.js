import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'

import RoleIcon from '../RoleIcon'

const Picture = styled.div`
  background-image: url(${props => props.src});
  background-size: 110%;
  background-position: 50% 10%;
  height: 110px;
  width: 80px;
  opacity: .8;
`

const Container = styled.div`
  position: relative;
  z-index: 0;
  background-color: #333;


  :before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1;
    height: 100%;
    border-top: solid 110px ${props => props.hover? '#171D24' : '#11151A' };
    border-right: solid 20px transparent;
  }

  :after {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    z-index: 1;
    height: 100%;
    border-bottom: solid 110px ${props => props.hover? '#171D24' : '#11151A' };
    border-left: solid 20px transparent;
  }
`

const Name = styled.div`
  position: absolute;
  bottom: 1.3em;
  right: 60px;
  z-index: 2;
  padding: .5em 1.3em;
  background-color: #212A33;
  border-radius: 4px;
  font-weight: 900
  color: white;
  text-align: center;
  white-space: nowrap;
`

const IconBorder = styled.div`
  position: absolute;
  right: 0;
  bottom: 15px;
  z-index: 2;
  border-radius: 100%;
  border: solid 6px transparent;
  background-color: rgb(40, 51, 61);
`

export default class ChampionPic extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Container hover={this.props.hover}>
        <Picture src={window.helpers.assets.img('champion-loading', 'id', this.props.champion.key)}/>
        <Name>{this.props.champion.name}</Name>
        <IconBorder>
          <RoleIcon white type={this.props.role} style={{height:'18px'}}/>
        </IconBorder>
      </Container>
    )
  }
}
