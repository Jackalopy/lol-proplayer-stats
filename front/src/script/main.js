import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import App from './App'
import store from './redux/store/index'
import { addArticle } from './redux/actions/index'

window.store = store
window.addArticle = addArticle

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Provider store={store}>
      <App data={window.data} style={{height: '100%'}}/>
    </Provider>,
    document.getElementById('app'));
})
