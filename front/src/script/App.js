import React from 'react'
import ReactDOM from 'react-dom'
import { connect, Provider } from 'react-redux'
import { setGameData } from './redux/actions'
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import { Layout, Icon, Tooltip } from 'antd'
import rp from 'request-promise'
import md5 from 'md5'

import SearchBar from './components/SearchBar'
import Menu from './components/Menu'

import ViewController from './components/ViewController'
import HomeContent from './components/contents/Home'
import SummonerContent from './components/contents/Summoner'

import assets from './helpers/assets'
import mapState from './helpers/mapState'

function mapDispatch(dispatch) {
  return {
    setGameData: gameData => {
      return dispatch(setGameData({version: gameData.version, champions: gameData.champions, items: gameData.items, perks: gameData.perks, spells: gameData.spells}))
    }
  }
}

class ConnectedApp extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      collapsed: true,
      user: null,
      view: 'home',
      searchHistory: []
    }

    window.helpers = {
      assets
    }

    this.toggleCollapse = this.toggleCollapse.bind(this)
    this.Summoner = this.Summoner.bind(this)
    this.Home = this.Home.bind(this)
    this.search = this.search.bind(this)
    this.updateUser = this.updateUser.bind(this)

    window.helpers.assets.data('version').then(serverVersion => {
      let localVersion = window.localStorage.getItem('version')
      console.log('Verifying Local Data...')

      if(localVersion == serverVersion) {
        console.log('Local and server are the same version. Verifying data stability...')
        if(!this.verifyData()) {
          console.log('Data has to be redownloaded...')
          return this.updateData()
          .then(() => {
            console.log('Data has been updated.')
            return data
          })
        }
        else {
          console.log('Data is up to date.')
          return {
            version: localStorage.getItem('version'),
            champions: JSON.parse(localStorage.getItem('champions')),
            items: JSON.parse(localStorage.getItem('items')),
            perks: JSON.parse(localStorage.getItem('perks')),
            spells: JSON.parse(localStorage.getItem('spells'))
          }
        }
      } else {
        return this.updateData()
          .then((data) => {
            console.log('Data has been updated.')
            return data
          })
      }
    }).then((gameData) => {
      props.setGameData(gameData)
    })

    let searchHistory = JSON.parse(window.localStorage.getItem('searchHistory'))
    if(Array.isArray(searchHistory)) {
      this.state.searchHistory = searchHistory
    }
  }

  verifyData() {
    let verification = 0

    let champions = localStorage.getItem('champions'),
        championsMd5 = localStorage.getItem('md5-champions'),
        items = localStorage.getItem('items'),
        itemsMd5 = localStorage.getItem('md5-items'),
        perks = localStorage.getItem('perks'),
        perksMd5 = localStorage.getItem('md5-perks'),
        spells = localStorage.getItem('spells'),
        spellsMd5 = localStorage.getItem('md5-spells')

    if(md5(champions) != championsMd5) return false
    if(md5(items) != itemsMd5) return false
    if(md5(perks) != perksMd5) return false
    if(md5(spells) != spellsMd5) return false
    else return true
  }

  updateData() {
    return window.helpers.assets.data('all').then(data => {

      localStorage.setItem('version', data.version)
      localStorage.setItem('champions', JSON.stringify(data.champions.data))
      localStorage.setItem('md5-champions', data.champions.md5)
      localStorage.setItem('items', JSON.stringify(data.items.data))
      localStorage.setItem('md5-items', data.items.md5)
      localStorage.setItem('perks', JSON.stringify(data.perks.data))
      localStorage.setItem('md5-perks', data.perks.md5)
      localStorage.setItem('spells', JSON.stringify(data.spells.data))
      localStorage.setItem('md5-spells', data.spells.md5)

      data = {
        version: data.version,
        champions: data.champions.data,
        items: data.items.data,
        perks: data.perks.data,
        spells: data.spells.data
      }

      return data
    })
  }

  toggleCollapse() {
    this.setState({collapsed: !this.state.collapsed})
  }

  search(value, region) {
    this.setState({
      user: value,
      view: 'summoner'
    })

    let searchHistory = this.state.searchHistory,
        index = searchHistory.indexOf(value)
    if(index == -1) {
      this.setState({searchHistory: [value].concat(searchHistory.splice(0, 19))}, () => {
        window.localStorage.setItem('searchHistory', JSON.stringify(this.state.searchHistory))
      })
    } else {
      searchHistory.splice(index, 1)
      this.setState({searchHistory: [value].concat(searchHistory.splice(0, 19))}, () => {
        window.localStorage.setItem('searchHistory', JSON.stringify(this.state.searchHistory))
      })
    }
  }

  updateUser() {
    rp({
      uri: `${document.location.origin}/data/summoner/by-puuid`,
      json: true,
      qs: {
        puuid: this.state.user.puuid,
        forced: true
      }
    })
    .then((user) => this.setState({user, view: 'summoner'}))
    .catch(console.error)
  }

  render() {
    return (
      <Router>
        <Layout style={{height: '100%', backgroundColor: 'rgb(40, 51, 61)'}} >
          <Layout.Sider
            collapsible
            collapsed={this.state.collapsed}
            trigger={null}
            theme="dark"
            style={{background: '#212A33'}}
          >
            <Layout.Header
              style={{
                background: 'transparent',
                color: 'hsla(0, 0%, 100%, .65)',
                whiteSpace: 'nowrap',
                transition: 'color .3s cubic-bezier(.645,.045,.355,1),border-color .3s cubic-bezier(.645,.045,.355,1),background .3s cubic-bezier(.645,.045,.355,1),padding .15s cubic-bezier(.645,.045,.355,1)',
                padding: 0
              }}
            >
              {this.state.collapsed?
                <Tooltip placement="right" title="Collapse Menu" onClick={this.toggleCollapse} style={{width: '100%', padding: 0}}>
                  <div style={{width: '100%', height: '100%', textAlign: 'center', cursor: 'pointer'}}>
                    <Icon type={this.props.collapsed? 'menu-unfold' : 'menu-fold'} />
                  </div>
                </Tooltip> :
                <div onClick={this.toggleCollapse} style={{cursor: 'pointer', padding: '0 25px'}}>
                  <Icon type={this.props.collapsed? 'menu-unfold' : 'menu-fold'} />
                  <span style={{
                    marginLeft: 10,
                    opacity: '.3s cubic-bezier(.645,.045,.355,1), width .3s cubic-bezier(.645,.045,.355,1)'
                  }}>
                    Collapse Menu
                  </span>
                </div>
              }
            </Layout.Header>
          <Menu onToggle={this.toggleCollapse} collapsed={this.state.collapsed} onSelect={(e) => {this.setState({section: e.key})}}/>
          </Layout.Sider>
          <Layout style={{backgroundColor: 'transparent'}}>
            <Layout.Header
              theme="dark"
              style={{
                position: 'relative',
                zIndex: 1,
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                background: 'transparent',
                width: 1000,
                maxWidth: '100%',
                margin: '0 auto',
                padding: 30
              }}
            >
              {this.state.user !== null? <Redirect to={'/summoner/name/' + this.state.user} /> : null}
              <Link to={'/'}><img src="/assets/img/ganqs_logo.svg" style={{maxHeight:25, maxWidth: `100%`, marginRight: 30}} /></Link>
              <SearchBar onSearch={this.search} dataSource={this.state.searchHistory}/>
            </Layout.Header>
            <Layout.Content style={{
              position: 'relative',
              zIndex: 0,
              display: 'table',
              width: 1000,
              maxWidth: '100%',
              minHeight: 'calc(100% - 100px)',
              margin: '0 auto',
              padding: 30,
              backgroundColor: 'rgb(40, 51, 61)'
            }}>
              <Route exact path='/' component={this.Home}/>
              <Route path='/summoner/name/:name' component={this.Summoner}/>
            </Layout.Content>
          </Layout>
        </Layout>
      </Router>
    )
  }

  Summoner({ match }) {
    return <SummonerContent player={match.params.name} update={this.updateUser}/>
  }

  Home({ match }) {
    return <HomeContent/>
  }
}

const App = connect(mapState, mapDispatch)(ConnectedApp)

export default App
