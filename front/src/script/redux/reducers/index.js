import { ADD_ARTICLE } from '../constants/action-types'
import { SET_GAME_DATA } from '../constants/action-types'

const initialState = {
  gameData: {
    version: null,
    champions: null,
    items: null,
    perks: null,
    spells: null
  }
}

function rootReducer(state = initialState, action) {
  switch(action.type) {
    case SET_GAME_DATA:
      return Object.assign({}, state, {
        gameData: {
          version: action.payload.version,
          champions: action.payload.champions,
          items: action.payload.items,
          perks: action.payload.perks,
          spells: action.payload.spells,
        }
      })
    default:
      return state
  }
}

export default rootReducer
