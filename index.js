const express = require('express'),
  bodyParser = require('body-parser'),
  path = require('path'),
  fs = require('fs'),
  assert = require('assert')
  app = express(),
  port = process.env.PORT || 3000,
  routes = require('./routes'),
  api = require('./controllers/api'),
  Mongo = require('./controllers/Mongo'),
  Data = require('./controllers/Data')

app.set('view engine', 'pug')
app.set('views', path.join(__dirname, '/front/build/view'))
app.use('/assets', express.static('front/build/assets'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

global.config = JSON.parse(fs.readFileSync(path.join(__dirname, 'config.json')))

let mongoConfig = global.config.mongo

mongoConfig.url = `${mongoConfig.service}://${mongoConfig.user}:${mongoConfig.pswd}@${mongoConfig.uri}`

const mongo = new Mongo(mongoConfig)

mongo.setup()
  .then(() => {
    const data = new Data(mongo)
    global.data = data
    routes(app, data)
    app.listen(port)
    console.log(`Server up on port ${port}`)
  })
  .catch(console.error)
